<?php
?>
<!DOCTYPE html>
<html>
    <head>
       <title>Steven Stanley Simple Code Example</title>
       <meta charset="UTF-8" />
       <meta name="description" content="A simple code example to show skills of OOP, HTML5, MySQL and PHP.">
       <meta name="keywords" content="steven stanley, code example">
       <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <header>
            <h1>Steven Stanley's Simple Code Example</h1>
        </header>
        <div id="mainContent">

        </div>
        <footer>
             Steven R. Stanley Copyright <?=date('Y') ?>
        </footer>
    </body>
</html>