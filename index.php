<!DOCTYPE html>
<html>
    <head>
        <title>Steven Stanley Example Index</title>
        <meta charset="UTF-8" />
        <meta name="description" content="A location for code examples">
        <meta name="keywords" content="steven stanley, code examples">
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <header>
            <h1>Steven Stanley's Code Examples</h1>
        </header>
        <div id="mainContent">
            <section>
                <h2>Links To Code Examples</h2>
                <ul>
                    <li><a href="example.php">Simple Code Example</a></li>
                </ul>
            </section>
        </div>
        <footer>
            Steven R. Stanley Copyright <?=date('Y') ?>
        </footer>
    </body>
</html>